package train;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;

public class BDD {
	
	private List<Train> listeTrains; 
	
	/** Actualise listeTrains selon les parametres de recherche en verifiant que le filtre n'est pas vide
	 * 
	 * Appelle les fonctions filtre de chaque parametre de recherche si le filtre n'est pas vide
	 * 
	 */
	public BDD () {
		this.listeTrains = (new Fichier()).ReadCSV();
	}

	List<Train> updateTrainsAvailable(Train trainFiltre) {
		List<Train> listeTrainsAvailable = new ArrayList<Train>();
		boolean v = true;
		for (int i = 0; i < listeTrains.size(); i++)
		{
			v = true;
			Train t =  this.listeTrains.get(i);
			if (trainFiltre.getGareDepart() != "")
				if (!t.getGareDepart().toLowerCase().equals(trainFiltre.getGareDepart().toLowerCase()))
					v = false;
			
			if (trainFiltre.getGareArrivee() != "" && v)
				if (!t.getGareArrivee().toLowerCase().equals(trainFiltre.getGareArrivee().toLowerCase()))
					v = false;

	
			if (trainFiltre.getDateDepart() != null && v)
				if (t.getDateDepart().compareTo(trainFiltre.getDateDepart()) != 0)
					v = false;

			if (trainFiltre.getDateArrivee() != null && v)
				if (t.getDateArrivee().compareTo(trainFiltre.getDateArrivee()) != 0)
					v = false;

			if (trainFiltre.getNbTickets() != -1 && v)
				if (t.getNbTickets() < trainFiltre.getNbTickets())
					v = false;

			if (trainFiltre.getClasse() != ClasseTrain.NULL && v)
				if (t.getClasse() != trainFiltre.getClasse())
					v = false;
			
			//PAS DE FILTRE SUR LE TYPE POUR LE MOMENT
/*
			if (trainFiltre.getType() != Type.NULL && v)
				if (t.getType() != trainFiltre.getType())
					v = false;
				*/
			
			if (v) {
				listeTrainsAvailable.add(t);
			}
		}
		return listeTrainsAvailable;
	}


	public Train  getIdTrainBDD(int idTrain) {
		for (int i = 0; i < this.listeTrains.size(); i++)
		{
			if (this.listeTrains.get(i).getIdTrain() == idTrain)
			{
				return this.listeTrains.get(i);
			}
		}
		return null;
	}
}
