package train;

import java.util.Date;

public class Train {

	private String gareDepart; // ""
	private String gareArrivee; // ""
	private Date dateDepart; //null
	private Date dateArrivee; //null
	private int nbTickets; //-1
	private ClasseTrain classe; //NULL
	private Type type; // NULL
	private int idTrain; //-1

	public Train(String gareDepart, String gareArrivee, Date dateDepart, Date dateArrivee, int nbTickets,
			ClasseTrain classe, Type type, int idTrain) {
		this.gareDepart = gareDepart;
		this.gareArrivee = gareArrivee;
		this.dateDepart = dateDepart;
		this.dateArrivee = dateArrivee;
		this.nbTickets = nbTickets;
		this.classe = classe;
		this.type = type;
		this.idTrain = idTrain;
	}

	public Train() {
		this.gareDepart = "";
		this.gareArrivee = "";
		this.dateDepart = null;
		this.dateArrivee = null;
		this.nbTickets = -1;
		this.classe = ClasseTrain.NULL;
		this.type = Type.NULL;
		this.idTrain = -1;
	}

	public String getGareDepart() {
		return gareDepart;
	}

	public void setGareDepart(String gareDepart) {
		this.gareDepart = gareDepart;
	}

	public String getGareArrivee() {
		return gareArrivee;
	}

	public void setGareArrivee(String gareArrivee) {
		this.gareArrivee = gareArrivee;
	}

	public Date getDateDepart() {
		return dateDepart;
	}

	public void setDateDepart(Date dateDepart) {
		this.dateDepart = dateDepart;
	}

	public Date getDateArrivee() {
		return dateArrivee;
	}

	public void setDateArrivee(Date dateArrivee) {
		this.dateArrivee = dateArrivee;
	}

	public int getNbTickets() {
		return nbTickets;
	}

	public void setNbTickets(int nbTickets) {
		this.nbTickets = nbTickets;
	}

	public ClasseTrain getClasse() {
		return classe;
	}

	public void setClasse(ClasseTrain classe) {
		this.classe = classe;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public int getIdTrain() {
		return idTrain;
	}

	public void setIdTrain(int idTrain) {
		this.idTrain = idTrain;
	}

}
