package train;

import java.util.List;
import java.util.Map;

import org.restlet.Component;
import org.restlet.Server;
import org.restlet.data.Protocol;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;
import java.io.IOException;
 
import org.restlet.resource.ClientResource;
import org.restlet.resource.ResourceException;
public class RecuperationRequete extends ServerResource {
	
	@Get  
	public String getTrainsAvailableWithFilters() {
		//Recuperation de tous les filtres
		try {
			Map<String, String> filtres = getQuery().getValuesMap();
			//Transformation et ajouts des filtres dans l'objet Train
			Train f = new Train();
			if(filtres.containsKey("gareDepart")) {
				f.setGareDepart((String) filtres.get("gareDepart") );
			}
			
			if(filtres.containsKey("gareArrivee")) {
				f.setGareArrivee((String) filtres.get("gareArrivee") );
			}
			
			if(filtres.containsKey("dateDepart")) {
				f.setDateDepart(Convert.convertStringToDate((String) filtres.get("dateDepart")));
			}
			
			if(filtres.containsKey("dateArrivee")) {
				f.setDateArrivee(Convert.convertStringToDate((String) filtres.get("dateArrivee")));
			}
			
			if(filtres.containsKey("classe")) {
				f.setClasse(Convert.convertStringToClasseTrain((String) filtres.get("classe")));
			}
	
			if(filtres.containsKey("type")) {
				f.setType(Convert.convertStringToType((String) filtres.get("type")));
			}
			
			if(filtres.containsKey("nbTickets")) {
				f.setNbTickets(Integer.valueOf((String) filtres.get("nbTickets")));
			}
			
			if(filtres.containsKey("idTrain")) {
				f.setIdTrain(Integer.valueOf((String) filtres.get("idTrain")));
			}
			
			//Recuperation de la liste de train possible 
			List<Train> listeTrains = (new BDD()).updateTrainsAvailable(f);
			
			return Convert.convertListTrainToJSON(listeTrains, f) ;
		} catch(Exception e) {
			System.err.println("err : "+e);
			return "erreur";
		}
	} 

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		// Create the HTTP server and listen on port 8182  
		//new Server(Protocol.HTTP, 8182, RecuperationRequete.class).start();
		Component component = new Component();
        // if you have an HTTP connector listening on the same port (i.e. 8182), you will get an error
        // in this case change the port number (e.g. 8183)
	    component.getServers().add(Protocol.HTTP, 8182);
	    // Then attach it to the local host  
	    component.getDefaultHost().attach("/getTrainsAvailableWithFilters", RecuperationRequete.class);  
	    component.getDefaultHost().attach("/reserveTrain", ReserverTrain.class);  

	
	    // Now, let's start the component!  
	    // Note that the HTTP server connector is also automatically started.  
	    component.start();  
	}
}
