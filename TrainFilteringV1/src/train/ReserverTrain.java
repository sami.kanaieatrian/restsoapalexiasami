package train;

import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

public class ReserverTrain extends ServerResource {
	
	@Get
	public String reserveTrain() {
		try {
			String str = getQuery().getValuesMap().get("idTrain");
			System.out.println("id = "+str);
			int idTrain = Integer.parseInt(str);
			System.out.println("train id = "+idTrain);
			BDD bdd = new BDD();
	        Train train = bdd.getIdTrainBDD(idTrain);   //R�cup�re le train par son ID.

	        if (train != null) {
	            // V�rifie si le train a des place disponibles.
	            if (train.getNbTickets() > 0) {
	                
	                train.setNbTickets(train.getNbTickets() - 1);
	                bdd.updateTrainsAvailable(train); // Met � jour le fichier.
	                return "Reservation reussie.";
	            } else {
	                return "Desole, il n'y a plus de places disponibles pour ce train.";
	            }
	        } else {
	            return "Train non trouve.";
	        }
		} catch(Exception e) {
			System.err.println("err : "+e);
			return "erreur : "+e;
		}
    }

}
