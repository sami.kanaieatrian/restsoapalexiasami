package train;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.io.FileReader;
import java.io.IOException;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;

public class Fichier {
	
	/**
	 * R�cup�re tous les trains avec des places libres du CSV 
	 * @return
	 */
	public List<Train> ReadCSV() {
	   List<Train> trains = new ArrayList<Train>();
	    try {
	    	CSVReader csvReader = new CSVReader(new FileReader("src\\train\\bdd.csv"));
	    
		    String[] values = null;
		    csvReader.readNext(); //Permet d'enlever l'en-t�te
		    while ((values = csvReader.readNext()) != null) {
		    	List<String> train = Arrays.asList(values[0].split(";"));
		    	//System.out.println("train = "+train);
		    	if(Integer.valueOf(train.get(4)) != 0) { // Si il n'y a plus de places on le skip
			    	Date dateDepart = Convert.convertStringToDate(train.get(2));
			    	Date dateArrivee = Convert.convertStringToDate(train.get(3));
			    	ClasseTrain classe = Convert.convertStringToClasseTrain(train.get(5)); 
			    	Type type = Convert.convertStringToType(train.get(6));
			    	int nbTickets = Integer.valueOf(train.get(4)); 
			    	trains.add(new Train(train.get(0), train.get(1), dateDepart, dateArrivee, nbTickets, classe, type, Integer.valueOf(train.get(7))));
		    	} 
		    }
		    csvReader.close();
	    }  catch (IOException | CsvValidationException | NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    return trains;
	}
}
