package train;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;




	public class Convert {

		public static Date convertStringToDate(String s) {
			try {
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm", Locale.ENGLISH);
				Date date = formatter.parse(s);
				return date;
			} catch (ParseException e) {
				e.printStackTrace();
				return null;
			}
		}
	 
	 public static ClasseTrain convertStringToClasseTrain(String s) {
		 switch(s) { 
			 case "PREMIERE" :
			 	return ClasseTrain.PREMIERE;
			 case "BUSINESS" :
				 return ClasseTrain.BUSINESS;
			 default : 
				 return ClasseTrain.STANDART;
		 }
	 }
	 
	 public static Type convertStringToType(String s) {
		 switch(s) {
		 	case "TGV" : 
		 		return Type.TGV;
		 	case "INTERCITE" : 
		 		return Type.INTERCITE;
		 	default : 
		 		return Type.TER;
		 }
	 }
	 
	 public static String convertListTrainToJSON(List<Train> listeTrains, Train filtres) {
		 System.out.println("Dans le convert; taille liste trains = "+listeTrains.size());
		 String json = "{ \n";
		 
		 json = Convert.convertFiltresToJSON(filtres, json);
		
		//Ajout des trains disponibles
		json += " \"Trains disponibles\": \n";
		json += "[ \n";
		for(int i=0; i<listeTrains.size(); i++) {
			Train t = listeTrains.get(i);
			json += "{ \n";
			json += "\"idTrain\": \""+Integer.toString(t.getIdTrain())+"\",\n";
			json += " \"gareDepart\": \""+t.getGareDepart()+"\",\n";
			json += " \"gareArrivee\": \""+t.getGareArrivee()+"\",\n";
			json += " \"dateDepart\": \""+t.getDateDepart().toString()+"\",\n";
			json += " \"dateArrivee\": \""+t.getDateArrivee().toString()+"\",\n"; 
			json += " \"classe\": \""+t.getClasse().name()+"\",\n";
			json += " \"type\": \""+t.getType().name()+"\",\n";
			json += "\"nbTickets\": \""+Integer.toString(t.getNbTickets())+"\",\n";
			json += "}";
			if(i != listeTrains.size()-1)
				json += ",";
			json += " \n";
		}
		json += "] \n";
		json += "}";
		System.out.println("json dans le convert= "+json);
		
		return json;
	 }
	 
	 private static String convertFiltresToJSON(Train filtres, String json) {
		//Ajout des filtres dans le json
			json += " \"filtres\": \n ";
			json += "{ \n ";
			json += " \"gareDepart\": \""+filtres.getGareDepart()+"\",\n";
			json += " \"gareArrivee\": \""+filtres.getGareArrivee()+"\",\n";
			
			if(filtres.getDateDepart() != null)
				json += " \"dateDepart\": \""+filtres.getDateDepart().toString()+"\",\n";
			else 
				json += " \"dateDepart\": \""+"\",\n";

			if(filtres.getDateArrivee() != null)
				json += " \"dateArrivee\": \""+filtres.getDateArrivee().toString()+"\",\n"; 
			else 
				json += " \"dateArrivee\": \""+"\",\n"; 
			
			json += " \"classe\": \""+filtres.getClasse().name()+"\",\n";
			json += " \"type\": \""+filtres.getType().name()+"\",\n";
			json += "\"idTrain\": \""+Integer.toString(filtres.getIdTrain())+"\",\n";
			json += "\"nbTickets\": \""+Integer.toString(filtres.getNbTickets())+"\"\n";
			json += " } \n";
			
			return json;
	 }
}
