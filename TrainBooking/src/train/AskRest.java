package train;
import java.io.BufferedReader;
import java.util.List;

import java.util.ArrayList;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
public class AskRest {
   
    public static List<String> getTrainsAvailableWithFiltersSoap(String[] filters) throws Exception {
    	//System.out.println("filters = "+filters.length);
    	 for(String s : filters) {
        	System.out.println("filters = "+s);
    	}
    	
    	//String[] filters = filter.split("=")[1].split(";");
    	
        String param = "";
        String[] nomParam = {"gareDepart", "gareArrivee", "dateDepart", "dateArrivee", "classe", "type", "nbTickets", "idTrain"};
        String[] valVide = {"", "", "null", "null", "NULL", "NULL", "-1", "-1"};
        
        for(int i=1; i<filters.length; i++) {
        	param = AskRest.verifIfExist(param, filters[i], valVide[i], nomParam[i]);
        }
        
        //Url en local host         URL url = new URL("https://localhost:8182/TrainFiltering/getTrainsAvailableWithFilters?"+param);
        
        URL url = new URL("http://localhost:8182/getTrainsAvailableWithFilters?"+param);
        URLConnection connection = url.openConnection();
 
        
        // `HttpURLConnection` connection = (HttpURLConnection) url.openConnection();
        // connection.setRequestMethod("GET");
        
        List<String> listeResultat = new ArrayList<String>();
        
        try (BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream())))
        {
            String line;
            for(int i=0; i<14; i++)
            	in.readLine();
            while ((line = in.readLine()) != null) {
            	listeResultat.add(line);
            }
        } catch (Exception e) {
            System.err.println("Erreur : Exception la lecture ne se fait pas : "+e);
        }
        return listeResultat;
   }
    
    private static String verifIfExist(String param, String filter, String isVide, String nomParam) {
    	//String nomFiltre = filter.split("=")[0];
    	//String valFiltre = filter.split("=")[1];
    	 if(filter != isVide)
    		 if (param != "")
    			 param += "&"+nomParam+"="+filter;
    		 else
    			 param += nomParam+"="+filter;
    	
    	 return param;
    }

    public static String reserveTrain(int trainId) throws Exception {
    	 URL url = new URL("http://localhost:8182/reserveTrain?"+"idTrain="+trainId);
         URLConnection connection = url.openConnection();
               
         try (BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream())))
         {
        	 String line = in.readLine();
             if (line != null) {
             	return line;
             }
         } catch (Exception e) {
             System.err.println("Erreur : Exception la lecture ne se fait pas : "+e);
         }
         return "test";
           
    }
}
