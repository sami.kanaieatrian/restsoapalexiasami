#Aléxia Bernard et Sami Kanie Atrian

### Lancement du projet

Ce projet est à ouvrir avec Eclipse. 

Il faut d'abord lancer le serveur SOAP en faisant un clic droit sur le projet TrainBooking puis Run As->on Server Tomcat. 
Ensuite, il faut lancer le côté REST en faisant un clic droit sur le projet TrainFiltering puis Run As->Java application.
Pour terminer, il faut lancer le client SOAP en faisant un clic droit sur le projet ClientSOA puis Run As->Java application. 

Voilà ! Notre travail est lancé maintenant vous pouvez le tester. 

### Requirements : 
1. Create REST Train Filtering service B	(6)
2. Create SOAP Train Booking service A	(4)
3. Interaction between two services	(4)
4. Test with Web service Client (instead of using Eclipse's Web service Explorer)	(2)
5. Work with complex data type (class, table, etc.)	(2)
6. Work with database (in text file, xml, in mysql, etc.)	(2)
7. Postman	(2)
8. OpenAPI	(3)
9. BPMS	(5)

#### Les points qu'on a traité : 1, 2, 3, 4, 5, 6, 7, 8

1. Create REST Train Filtering service B	(6/6)
2. Create SOAP Train Booking service A	(4/4)
3. Interaction between two services	(4/4)
4. Test with Web service Client (instead of using Eclipse's Web service Explorer)	(1/2)
5. Work with complex data type (class, table, etc.)	(2/2)
6. Work with database (in text file, xml, in mysql, etc.)	(2/2)
7. Postman	(1/2)
8. OpenAPI	(1,5/3)
9. BPMS	(0/5)
